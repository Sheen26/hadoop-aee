# Example AEE

library(rmr2)

# Airline on-time data available from
# http://stat-computing.org/dataexpo/2009/the-data.html
# description:
# Column  Name                         Description
# 1       ID                           The sequential record identifier
# 2       Year                         1987-2008
# 3       Month                        1-12
# 4       DayofMonth                   1-31
# 5       DayOfWeek                    1 (Monday) - 7 (Sunday)
# 6       DepTime                      actual departure time (local, hhmm)
# 7       CRSDepTime                   scheduled departure time (local, hhmm)
# 8       ArrTime                      actual arrival time (local, hhmm)
# 9       CRSArrTime                   scheduled arrival time (local, hhmm)
# 10      UniqueCarrier                unique carrier code
# 11      FlightNum                    flight number
# 12      TailNum                      plane tail number
# 13      ActualElapsedTime            in minutes
# 14      CRSElapsedTime               in minutes
# 15      AirTime                      in minutes
# 16      ArrDelay                     arrival delay, in minutes
# 17      DepDelay                     departure delay, in minutes
# 18      Origin                       origin IATA airport code
# 19      Dest	                       destination IATA airport code
# 20      Distance	                   in miles
# 21      TaxiIn                       taxi in time, in minutes
# 22      TaxiOut                      taxi out time in minutes
# 23      Cancelled                    was the flight cancelled?
# 24      CancellationCode             reason for cancellation (A = carrier, B = weather, C = NAS, D = security)
# 25      Diverted                     1 = yes, 0 = no
# 26      CarrierDelay                 in minutes
# 27      WeatherDelay                 in minutes
# 28      NASDelay                     in minutes
# 29      SecurityDelay                in minutes
# 30      LateAircraftDelay            in minutes

# Simple logistic regression model:
# (ArrDelay > 15) ~ DayOfWeek + Distance

# Change value here to determine number of partitions
get_k <- function() {
  return(10)
}

expit <- function(x) {
  return(exp(x) / (1 + exp(x)))
}

# Map - split records according to number of partitions k
# (divide part of 'divide-and-conquer')
# @param k - NULL
# @param v - character vector - each line as separate element
split.by.k.map.fn <- function(k, v) {
  # creates a list of character vectors
  records = strsplit(x = v, split = ",")
  # remove header row if present
  if (records[[1]][1] == "ID") {
    records[1] = NULL
  }
  # convert to data.frame
  records = data.frame(t(sapply(records, c)), stringsAsFactors = FALSE)
  # ID is column 1
  partition.key = as.integer(records[, 1]) %% get_k()
  # DayOfWeek is column 5
  # ArrDelay is column 16
  # Distance is column 20
  x.val = data.frame(
    ID = as.integer(records[, 1]),
    DayOfWeek = as.integer(records[, 5]),
    ArrDelay = as.numeric(records[, 16]),
    Distance = as.numeric(records[, 20])
  )
  keyval(partition.key, x.val)
}

# Reduce - compute local AEE
# Compute (A_k, B_k)
# k - integer vector of length 1 - the partition identifier
# v - data.frame with columns:
#   $ID - Integer record identifier for each value
#     (not used for production, but useful for debugging)
#   $DayOfWeek - Integer in 1-7 for day of week
#   $ArrDelay - Numeric arrival delay in minutes
#   $Distance - Numeric flight distance in miles
local.aee.reduce.fn <- function(k, v) {
  # Omit NAs
  v = v[complete.cases(v), ]
  # Determine flight status
  v$Flight.Status = as.integer(v$ArrDelay > 15)
  # Convert DayOfWeek to factor
  v$DayOfWeek = as.factor(v$DayOfWeek)
  # Make sure all levels of DayOfWeek exist to get proper dummy
  # variable covariates within model matrix
  levels(v$DayOfWeek) = as.character(1:7)
  # Obtain design matrix with dummy variables for DayOfWeek
  # Each row of this matrix is x_i vector in AQLE estimator
  X = model.matrix(~ Distance + DayOfWeek, data = v)
  # Fit logistic model to obtain B_k
  fit = glm(Flight.Status ~ Distance + DayOfWeek,
            data = v,
            family = "binomial")
  B_hat = coef(fit)
  # m = number of covariates including intercept
  m = length(B_hat)
  # n = number of samples within this partition
  n = nrow(v)
  A = matrix(data = 0,
             nrow = m,
             ncol = m)
  X.rep = A # pre-alloc
  Q.rep = A # pre-alloc
  for (i in 1:n) {
    # X.rep is x.i covariate vector replicated across m rows
    # X.rep = (note x.i1 = 1 for the intercept)
    #      [,1]  [,2]       [,m]
    # [1,] 1     x.i2  ...  x.im
    # [2,] 1     x.i2  ...  x.im
    # ...
    # [m,] 1     x.i2  ...  x.im
    X.rep = X[rep(i, times = m),]
    # Q.rep is matrix of partial derivatives of expit wrt B_hat_j
    # q.j = d/dBj{expit(dot(x_i, B_hat))} = expit * (1 - expit) * x_ij
    #      [,1]    [,2]    [,m]
    # [1,] q.1     q.2     ... q.m
    # [2,] q.1     q.2     ... q.m
    #      ...
    # [m,] q.1     q.2     ... q.m
    q = expit(sum(X[i, ] * B_hat))
    q = q * (1 - q)
    Q.rep = q * X.rep
    A = A + (Q.rep * t(X.rep))
  }
  
  aee.out = list()
  aee.out[[as.character(k)]] = list(A = A, B_hat = B_hat)
  keyval(k, aee.out)
}

# Available inputs with rmr.options(backend="hadoop")
# 1.) "/airline.keyed.csv" - The full multi-GB airline data set with 100+ million records
#   *not available on linked virtual machine*
# 2.) "/airline.trim.keyed.csv" - Only the first 1000 records of airline data
# 3.) "/airline.trim2.keyed.csv" - Only the first 1,000,000 records of airline data
# 4.) "/airline.trim3.keyed.csv" - Only the first 10,000,000 records of airline data
# - Careful when using large data sets as may recieve cryptic error and YARN crash due
#   to disk space running out from transient hadoop files.
# - Also, don't forget to change get_k() when switching datasets
# - To test local in-memory, call: rmr.options(backend="local") and
#   update path to input data to local disk instead of hdfs
#   (e.g ~/Desktop/setup_files/airline.trim.keyed.csv)
do.aee <- function(input="/airline.trim.keyed.csv") {
  start_time = proc.time()
  # Run map-reduce
  aee.run = mapreduce(
    input = input,
    map = split.by.k.map.fn,
    reduce = local.aee.reduce.fn,
    combine = NULL,
    input.format = "text",
    output.format = "native",
    verbose = TRUE
  )
  
  # Extract results from hdfs
  # result is a list with members
  #   $key - numeric vector of emitted keys (in this case each key is the partition id 'k' as a character)
  #   $val - list of values emitted. Each value (1 per key) is actually
  #     a list with members
  #     $A - The aggregate estimation matrix for the k-th partition
  #     $B_hat - The local coefficient estimates for the k-th partition
  result = from.dfs(input = aee.run, format = "native")
  
  # Aggregate partition estimators to obtain final estimate
  num_k = length(result$val)
  print(paste0("Estimating AEE from ", num_k, " partitions."))
  # Equation (4) in AEE paper
  #stopifnot(num_k >= 1)
  m = length(result$val[[1]]$B_hat)
  Sum_A_inv = matrix(0, nrow = m, ncol = m)
  Sum_A_mult_B_hat = rep(0, m)
  for (k in 1:num_k) {
    A_k = result$val[[k]]$A
    B_hat_k = result$val[[k]]$B_hat
    Sum_A_inv = Sum_A_inv + A_k
    Sum_A_mult_B_hat = Sum_A_mult_B_hat + (A_k %*% B_hat_k)
  }
  # Compute inverse
  Sum_A_inv = solve(Sum_A_inv)
  # Compute final coefficients
  final_B_hat = Sum_A_inv %*% Sum_A_mult_B_hat
  print(proc.time() - start_time)
  return(final_B_hat)
}
